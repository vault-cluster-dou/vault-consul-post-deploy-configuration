###  Ubuntu

#### Get Hosts

To get a list of hosts, run the following.

Connect to the Bastion Host:


    ssh -i <key_location> ubuntu@<your_bastion_host_ip_address_goes_here>  ##  e.g. ssh -i ~/.ssh/id_rsa_consul ubuntu@54.45.13.37


Set up the Bastion Host:

    sudo apt update
    sudo apt upgrade -y
    sudo apt-add-repository ppa:ansible/ansible
    sudo apt update
    sudo apt install -y ansible
    sudo apt install -y python-pip
    pip install boto
    ansible --version

The above includes how to install Ansible 2.8, from the following guide: https://www.techrepublic.com/article/how-to-install-ansible-on-ubuntu-server-18-04/

If your version is lower than 2.8, make a quick issue or refer to the above guide.

Get Ansible code onto your Bastion Host:

    git clone git@github.com:<YOUR_GITHUB_USERNAME>/IS-terraform-aws-consul-enterprise-ansible.git

Replace `<YOUR_GITHUB_USERNAME>` with your Github username, after you have forked the https://github.com/hashicorp/IS-terraform-aws-consul-enterprise-ansible repository.

Set up your Ansible configuration:

Modify `ansible.cfg` to show the following:

    remote_user = ubuntu

Set up your Ansible Inventory:

    vi ec2.ini

Example (first 10 lines):


    ; Documentation on all settings available at
    ; https://github.com/ansible/ansible/blob/devel/contrib/inventory/ec2.ini

    [ec2]

    regions = us-west-2
    vpc_destination_variable = private_ip_address

    stack_filters = False
    instance_filters = vpc-id=vpc-8f834deadfeed1337&tag:AnsibleManaged=True


Replace the parts in `<>` with values you're working with, for example, `<VPC_ID>` would get replaced with `vpc-8f834deadfeed1337` if that's your VPC id, and `<REGION>` would get replaced with `us-west-2` if you're on the `us-west-2` region.

Authenticate to Amazon:

    export AWS_ACCESS_KEY_ID=KIATOY2KFUFFHITM6VAA
    export AWS_SECRET_ACCESS_KEY=1XeOVsajFuqwHaTE4DGotApEipWC81J9fAqp7wzn

Get your list of hosts:

    python hosts/ec2.py --list | grep ansible_host

Should show something like this as output:

        "ansible_host": "10.66.6.11",
        "ansible_host": "10.66.6.42",
        "ansible_host": "10.66.6.46",
        "ansible_host": "10.66.6.73",
        "ansible_host": "10.66.6.74",

Make a note of each of these, because you'll need to SSH to one of them to verify some things.

Try an Ansible run, because this will check whether you can connect to Ansible to your systems!

    ~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ ansible-playbook configuration.yaml --key-file=<YOUR_KEY_FILE_GOES_HERE>  ##  e.g. ~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ ansible-playbook configuration.yaml --key-file=~/.ssh/id_rsa_consul

Most likely the first run will fail, because we haven't set up key files yet. But this shows that we are ready for the next steps.

Now try it with variables.

Here's an example, using made-up names:  

    ansible-playbook configuration.yaml --key-file=~/.ssh/id_rsa_consul --extra-vars '{"consul_join_tag_key":"quorfgames-consul-dev-joinkey","consul_join_tag_value":"quorfgames-consul-dev-joinval","consul_join_region":"us-west-2","consul_bootstrap_expect":"3","consul_agent_policy_name":"consul_agent_policy"}'

This will also fail, but not as much. You now need to create some TLS certificates for Consul.

Connect to one of the Consul hosts, and check that it has `consul`:

    $ ssh -i <key_location> ubuntu@<your_consul_host_ip_address_goes_here>  ##  e.g. ssh -i ~/.ssh/id_rsa_consul ubuntu@10.66.13.37
    $ which consul

Next, we'll use Consul on that host to generate a Consul TLS certificate and key, generate server certificates and keys, then place it in the appropriate `files/` directory within your Ansible folder tree, and run the Ansuble role again.

It should now succeed.

If it doesn't, please provide all of your non-secret output, and how we would reproduce the issue, in an Issue in our Git repository's hosting page!

We welcome your comments and concerns, because it will help us improve.

The following named tasks need key and certificate files:

https://github.com/v6/IS-terraform-aws-consul-enterprise-ansible/blob/master/contrib/ansible/roles/consul-agent/tasks/main.yml#L13-L43

Following instructions from here: https://learn.hashicorp.com/consul/security-networking/certificates#step-1-create-a-certificate-authority

```
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ ssh ubuntu@10.66.6.7
ubuntu@ip-10-66-6-7:~$ consul tls ca create
==> Saved consul-agent-ca.pem
==> Saved consul-agent-ca-key.pem
ubuntu@ip-10-66-6-7:~$ exit
logout
Connection to 10.66.6.7 closed.
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ scp ubuntu@10.66.6.7:~/consul-agent-ca.pem .
consul-agent-ca.pem                                                                                  100% 1249     1.8MB/s   00:00    
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ scp ubuntu@10.66.6.7:~/consul-agent-ca-key.pem .
consul-agent-ca-key.pem                                                                              100%  227   383.0KB/s   00:00    
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$
```

Exmaple of creating a Server certificate:

```
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ ssh ubuntu@10.66.6.7
ubuntu@ip-10-66-6-7:~$ consul tls cert create -server
==> WARNING: Server Certificates grants authority to become a
    server and access all state in the cluster including root keys
    and all ACL tokens. Do not distribute them to production hosts
    that are not server nodes. Store them as securely as CA keys.
==> Using consul-agent-ca.pem and consul-agent-ca-key.pem
==> Saved dc1-server-consul-0.pem
==> Saved dc1-server-consul-0-key.pem
ubuntu@ip-10-66-6-7:~$ exit
logout
Connection to 10.66.6.7 closed.
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ scp ubuntu@10.66.6.7:~/dc1-server-consul-0.pem .
dc1-server-consul-0.pem                                                                              100% 1139     1.7MB/s   00:00    
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ scp ubuntu@10.66.6.7:~/dc1-server-consul-0-key.pem .
dc1-server-consul-0-key.pem                                                                          100%  227   350.7KB/s   00:00    
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$ ls
ansible.cfg         consul-agent-ca-key.pem  dc1-server-consul-0-key.pem  ec2.py.ini  id_rsa_consul
configuration.yaml  consul-agent-ca.pem      dc1-server-consul-0.pem      hosts       roles
ubuntu@ip-10-66-6-152:~/IS-terraform-aws-consul-enterprise-ansible/contrib/ansible$
```

Keep going, running `ssh <SERVER_IP_ADDRESS_HERE> consul tls cert create -server`,
e.g. `ssh 10.66.6.74 consul tls cert create -server` on
the same consul node that you generated your `consul-agent-ca.pem` file
on, until you have 5 sets of server certificates.

Then, run `ssh <SERVER_IP_ADDRESS_HERE> consul tls cert create -client` 5 times on that same server.

Copy all of the .pem files back with `scp ubuntu@<SERVER_IP_ADDRESS_HERE>:~/dc1-client-consul-*.pem`

You should only have to do this on one server, and you'll end up with this:

```
$ ls
README.md                    dc1-client-consul-1-key.pem  dc1-client-consul-4.pem      dc1-server-consul-3-key.pem
ansible.cfg                  dc1-client-consul-1.pem      dc1-server-consul-0-key.pem  dc1-server-consul-3.pem
configuration.yaml           dc1-client-consul-2-key.pem  dc1-server-consul-0.pem      dc1-server-consul-4-key.pem
consul-agent-ca-key.pem      dc1-client-consul-2.pem      dc1-server-consul-1-key.pem  dc1-server-consul-4.pem
consul-agent-ca.pem          dc1-client-consul-3-key.pem  dc1-server-consul-1.pem      hosts
dc1-client-consul-0-key.pem  dc1-client-consul-3.pem      dc1-server-consul-2-key.pem  roles
dc1-client-consul-0.pem      dc1-client-consul-4-key.pem  dc1-server-consul-2.pem
```

Now put them all in the `tls/` folder of your Ansible source code directory on the Bastion host:

    cp *.pem roles/consul-agent/files/etc/consul.d/tls/

Run the Ansible command again with the variables mentioned above, and you should see correct installations of TLS certificates for each of your Consul nodes.

Example Ansible command with made-up variable names:

    ansible-playbook configuration.yaml --key-file=~/.ssh/id_rsa_consul --extra-vars '{"consul_join_tag_key":"quorfgames-consul-dev-joinkey","consul_join_tag_value":"quorfgames-consul-dev-joinval","consul_join_region":"us-west-2","consul_bootstrap_expect":"3","consul_agent_policy_name":"consul_agent_policy"}'

## ANSIBLE USAGE SUGGESTIONS

Create TLS certificates by hand, if needed, using the guide here:

https://learn.hashicorp.com/consul/security-networking/certificates
