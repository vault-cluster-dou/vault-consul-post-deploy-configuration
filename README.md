# ANSIBLE USAGE GUIDE & SUGGESTIONS

##  Prerequisites

This guide assumes that you've forked the repository at https://github.com/hashicorp/IS-terraform-aws-consul-enterprise-ansible

You should already have successfully run Terraform Apply to create AWS resources for Consul.

Make sure that all of your Consul instances have the following tag assigned to them:

    AnsibleManaged

And that the tag has the following value:

    True


## ADDITIONAL SETUP INSTRUCTIONS
- [Ubuntu](./docs/ubuntu.md)


## ANSIBLE USAGE GUIDE

To run:

`ansible-playbook bootstrap.yml`

Note: The directory `hosts` is specified by the inventory paramater in the ansible.cfg file. Files in the inventory directory have values specific to the Vault cluster (vpc, region, join tags, etc). Likely, multiple directories would exist to to represent values for different clusters. The directory to use can also be specified with a -i flag during run time.

Example:
```
ansible-playbook bootstrap.yml -i hosts
```

The following files need the placeholders replaced with appropriate values:

`ansible.cfg` :
 - `<REMOTE_USER>`: the user that will be used to ssh to instances (ex: `ubuntu` or `ec2-user`)

`hosts/ec2.ini`:
- `<REGION>` : Region of the nodes in the Vault cluster (ex: `us-east-1`)
- `<VPC_ID>` : the VPC where the nodes in the cluster reside (ex: `vpc-0000000000000`)

`hosts/group_vars/all.yaml`
- `<JOIN_TAG_KEY>` : The tag key for Consul cloud auto-join. This value should match the
value used in the terraform module for the `join_tag_key` variable. (ex: `ConsulClusterJoin`)
- `<JOIN_TAG_VALUE>` : The tag value used for Consul cloud auto-join. This value should match
the value used in the terraform module for the `join_tag_value` variable. Could be the cluster name (ex: `dev_us_001`)
- `<JOIN_REGION>` : Region of the nodes in the Vault cluster
- `<PYTHON>`: Depending on OS and version this could be `python2` or `python3`
- `<CONSUL_ENCRYPTION_KEY>`: This is a value that needs to be generated and fed in. It can be produced by running consul keygen on any host with the consul binary installed.

`hosts/group_vars/tag_VaultClusterNodeType_VaultServer.yaml`
- `<KMS_ID>`: key for Auto-Unseal (TODO - needs to be optional)
